odoo.define('vlr_appraisal_bulk.bulk_appraisal', function (require) {
"use strict";
var FormController = require('web.FormController');

FormController.include({
    init: function (parent, model, renderer, params) {
        this._super.apply(this, arguments);
    },

    renderButtons: function($node) {
        this._super.apply(this, arguments);
//        console.log('------ render buttons --------')
//        console.log(this)
//        if (this.modelName== 'hr.appraisal'){
//            if (this.initialState.data.bulk_line != false) {
//                console.log('masuk hr appraisal')
//                this.$buttons.find(".o_form_button_edit").hide();
//            }
//        } else {
//            console.log('nope')
//        }
    },

    getSelectedIds: function () {
        // Enhancement to hide appraisal form which under bulk appraisal
        var currentRecord = this.model.get(this.handle).data;
        this.$buttons.find(".o_form_button_edit").show();
        if (this.modelName == 'hr.appraisal') {
            if (currentRecord.bulk_line != false) {
                this.$buttons.find(".o_form_button_edit").hide();
            }
        }
        //  Existing code
        var env = this.model.get(this.handle, {env: true});
        return env.currentId ? [env.currentId] : [];
    },

});

});