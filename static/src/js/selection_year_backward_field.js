odoo.define('vlr_appraisal_bulk.selection_year_backward', function (require) {
"use strict";

var AbstractField = require('web.AbstractField');
var core = require('web.core');
var field_registry = require('web.field_registry');
var field_utils = require('web.field_utils');

var QWeb = core.qweb;

var SelectionYearOnwardWidget = AbstractField.extend({
    template: 'FieldSelection',
    events: _.extend({}, AbstractField.prototype.events, {
        'change': '_onChange',
    }),
    init: function () {
        this._super.apply(this, arguments);
        this._setValues();
    },
    getFocusableElement: function () {
        return this.$el.is('select') ? this.$el : $();
    },
    isSet: function () {
        return this.value !== false;
    },
    _getCurrentYearBackward: function () {
        var list = [];
        var currentYear = new Date().getFullYear();
        list.push(currentYear);
        for (var i=0; i < 4; i++){
            currentYear -= 1
            list.push(currentYear)
        }
        return list
    },
    _renderEdit: function () {
        this.$el.empty();
        var list_year = this._getCurrentYearBackward();
        for (var i=0; i < list_year.length; i++) {
            this.$el.append($('<option/>', {
                value: list_year[i],
                text: list_year[i],
            }));
        }

        var value = this.value;
        if (this.field.type === 'many2one' && value) {
            value = value.data.id;
        }
        this.$el.val(JSON.stringify(value));
    },
    _renderReadonly: function () {
        this.$el.empty().text(this._formatValue(this.value));
    },
    _reset: function () {
        this._super.apply(this, arguments);
        this._setValues();
    },

    _setValues: function () {
        if (this.field.type === 'many2one') {
            this.values = this.record.specialData[this.name];
            this.formatType = 'many2one';
        } else {
            this.values = _.reject(this.field.selection, function (v) {
                return v[0] === false && v[1] === '';
            });
        }
        this.values = [[false, this.attrs.placeholder || '']].concat(this.values);
    },


    //--------------------------------------------------------------------------
    // Handlers
    //--------------------------------------------------------------------------
    _onChange: function () {
//        var res_id = JSON.parse(this.$el.val());
        var res_id = this.$el.val();
        this._setValue(res_id);
    },


});
field_registry.add('selection_year_backward', SelectionYearOnwardWidget);
});

