odoo.define('vlr_appraisal_bulk.form_widgets', function (require) {
	"use strict";

	var core = require('web.core');
	//var form_common = require('web.form_common');
	var _t = core._t;
	var QWeb = core.qweb;
	//var Model = require('web.Model');
	var fieldRegistry = require('web.field_registry');
	var FieldOne2Many = fieldRegistry.get('one2many');


	var One2ManySelectable = FieldOne2Many.extend({
		// my custom template for unique char field
		template: 'One2ManySelectable', 

		multi_selection: true,
		//button click
		events: {
			"click .cf_button_confirm": "action_selected_lines",
		},
		start: function() 
	    {
	    	this._super.apply(this, arguments);
			var self=this;
			this.render_value();
		   },

		_format: function (row_data, options) {
            var value = row_data[this.id].value;
            // do something
            var style = 'red';
            return QWeb.render('One2ManySelectable', {
                 'style': style,
            });
        },
		render_value: function ()
		{
			//this._super.apply(this, arguments);
            var self=this;
            var tdCnt=0;
             this.$el.find('table.o_list_view tr').each(function () {
             	$(this).find('th').eq(0).after('<th width="1" class="o_list_record_selector"><div class="o_checkbox">' +
					'<input type="checkbox"><span></span></div></th>');
			 });
            this.$el.find('table.o_list_view tr.o_data_row').each(function () {

            	$(this).find('td').eq(0).after('<td class="o_list_record_selector"><input type="checkbox"></input></td>');

        //     	$('<input />', {
        //     type : 'checkbox',
        //     id : 'td' + tdCnt,
        //     class : 'dt-checkboxes',
        //     value : name
        // }).appendTo($(this).find("td").eq(0));
        tdCnt++;
			});

            this.$el.find('td.o_data_cell')
					.closest('tr').each(function () {
						//fourth create checkbox in that td element




						//$("<td />").html('<input type="checkbox"/>').appendTo($(this));
						// var chkbox = document.createElement('input');
    					// chkbox.type = "checkbox";
    					// chkbox.id = "chk" ;
    					// chkbox.name = "chk" ;
						//
						// ids.push(parseInt($(this).context.dataset.id));
						// console.log(ids);
			});


			if (this.get('readonly')) {
				self.$('.mass_delete_buttons').hide();
			}
			else{
				self.$('.mass_delete_buttons').show();
			}
			self.$el.find(".button_mass_delete").click(function(){
					self.delete_selected_lines();
	        });
			self.$el.find(".button_mass_select").click(function(){
	        	self.selected_lines();
	        });
		},
		//passing ids to function
		action_selected_lines: function()
		{		
			var self=this;
			var selected_ids = self.get_selected_ids_one2many();
			if (selected_ids.length === 0)
			{
				this.do_warn(_t("You must choose at least one record."));
				return false;
			}
			//var model_obj=new Model(this.dataset.model);
			//you can hardcode model name as: new Model("module.model_name");
			//you can change the function name below
			/* you can use the python function to get the IDS
			      @api.multi
				def bulk_verify(self):
        				for record in self:
            				print record
			*/
			//model_obj.call('bulk_verify',[selected_ids],{context:self.dataset.context})
			//.then(function(result){
			//});
		},
		//collecting the selected IDS from one2manay list
		get_selected_ids_one2many: function ()
		{
			var ids =[];
			this.$el.find('td.o_list_record_selector input:checked')
					.closest('tr').each(function () {
						
						ids.push(parseInt($(this).context.dataset.id));
						console.log(ids);
			});
			return ids;
		},


	});
	// register unique widget, because Odoo does not know anything about it
	//you can use <field name="One2many_ids" widget="x2many_selectable"> for call this widget
	//core.form_widget_registry.add('one2many_selectable', One2ManySelectable);
	fieldRegistry.add('one2many_selectable', One2ManySelectable);
	 return {
        One2ManySelectable: One2ManySelectable
    };
});
