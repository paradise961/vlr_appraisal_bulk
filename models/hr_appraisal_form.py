from odoo import api, fields, models, _
import datetime
import dateutil.parser
from odoo.exceptions import UserError, AccessError, ValidationError
from dateutil.relativedelta import relativedelta
from itertools import groupby



class HrBulkAppraisal(models.Model):
    _name = 'hr.appraisal_bulk'
    _inherit = ['mail.thread']

    def _get_default_bulk_appraisal_year(self):
        return datetime.datetime.today().year

    name = fields.Char()
    state = fields.Selection([
        ('draft', 'Draft'),
        ('exception', 'Sent Failed'),
        ('received', 'Received'),
        ('done', 'Finish')
    ], default='draft')
    user_id = fields.Many2one(comodel_name='res.users', string='Assigned to', default=lambda self: self.env.uid,
                              index=True, track_visibility='onchange')
    hr_customer_id = fields.Many2one(comodel_name='res.partner', string="Customer's Name", domain=[('customer', '=', True)],
                                     required=True)
    appraisal_deadline = fields.Date(string="Appraisal Deadline", required=True)
    announcement_id = fields.Many2one(comodel_name='hr.employee', string='Appraisal reviewer', required=True)
    customer_survey_id = fields.Many2one(comodel_name='survey.survey', string='Survey Form', required=True)
    hr_bulk_appraisal_ids = fields.One2many('hr.appraisal.bulk.line', 'appraisal_partner_id', string='Employee',
                                             help='This is auto generated line, the data is getting from timesheet '
                                                  'for particular customer on certain period')
    frequency = fields.Selection([
        ('1', 'Monthly'),
        ('2', 'Quarterly')
    ], string='Frequency', default='1', required=True)
    year = fields.Integer(default=_get_default_bulk_appraisal_year, required=True)
    month = fields.Selection([
        ('1', 'January'), ('2', 'February'), ('3', 'March'), ('4', 'April'), ('5', 'May'), ('6', 'June'),
        ('7', 'July'), ('8', 'August'), ('9', 'September'), ('10', 'October'), ('11', 'November'), ('12', 'December')
    ], string='Month')
    quarter_select = fields.Selection([
        ('1', 'Quarter 1'), ('2', 'Quarter 2'),
        ('3', 'Quarter 3'), ('4', 'Quarter 4')
    ], string='Quarter', help='Quarter 1 start from 1 Jan to 31 March\n'
            'Quarter 2 start from 1 April to 30 June\n'
            'Quarter 3 start from 1 July to 30 September\n'
            'Quarter 4 start from 1 October to 31 December')
    year_quarter = fields.Char(compute='_compute_year_quarter', help='in YYYYQQ format', store=True)
    start_period = fields.Date('Start Date', index=True, compute='_compute_start_end_period')
    end_period = fields.Date('End Date', index=True, compute='_compute_start_end_period')
    failure_reason = fields.Char(string='Last Failure Reason',help='this always provide information regarding latest fail sent email')

    appraisal_ids = fields.One2many(comodel_name='hr.appraisal', compute='_compute_appraisal_ids')
    appraisal_count = fields.Float(compute='_compute_appraisal_ids')

    @api.constrains('frequency', 'month', 'quarter_select' , 'year', 'appraisal_deadline')
    def _check_period_and_deadline(self):
        today_date = datetime.datetime.today().date()
        for bulk in self:
            if bulk.end_period >= str(today_date):
                raise ValidationError(_('Cannot Create/Edit appraisal bulk for end date greater than today date'))

            if bulk.appraisal_deadline <= str(today_date):
                raise ValidationError(_('The deadline cannot be in the past'))

            if bulk.end_period >= bulk.appraisal_deadline:
                raise ValidationError(_('Deadline should be greater than end period'))

    @api.depends('hr_bulk_appraisal_ids.appraisal_id')
    def _compute_appraisal_ids(self):
        for bulk in self:
            ids = []
            for bulk_line in bulk.hr_bulk_appraisal_ids:
                if bulk_line.appraisal_id:
                    ids.append(bulk_line.appraisal_id)

            bulk.appraisal_ids = ids
            bulk.appraisal_count = len(ids)

    @api.depends('start_period', 'end_period', 'year', 'frequency')
    def _compute_year_quarter(self):
        for bulk in self:
            if bulk.frequency == '1': #Monthly
                quarter = (int(bulk.month) - 1) // 3 + 1
            elif bulk.frequency == '2': #Quarterly
                quarter = bulk.quarter_select

            YYYYQQ = ('%(year)sQ%(quarter)s') % {'year': bulk.year, 'quarter': quarter}
            bulk.year_quarter = YYYYQQ

    @api.multi
    def write(self, vals):
        ## Disallow delete employee/bulkline when state is not draft
        res = super(HrBulkAppraisal, self).write(vals)
        if any([field_name in vals for field_name in ['hr_bulk_appraisal_ids']]):
            for bulk in self:
                if bulk.state != 'draft':
                    raise UserError(_('Cannot delete Employee/ Bulk Line not on state draft'))

        return res

    @api.depends('year', 'month', 'frequency', 'quarter_select')
    def _compute_start_end_period(self):
        for record in self:
            if record.year and record.frequency and (record.month or record.quarter_select):
                start_date = '1'
                number_of_month = 0
                if record.frequency == '1':
                    month = record.month
                    number_of_month = 1
                elif record.frequency == '2':
                    number_of_month = 3
                    if record.quarter_select == '1': month = '1'
                    elif record.quarter_select == '2': month = '4'
                    elif record.quarter_select == '3': month = '7'
                    elif record.quarter_select == '4': month = '10'

                start_date_string = ('%(year)s-%(month)s-%(day)s') % {
                    'year': record.year,
                    'month': month,
                    'day': start_date
                }
                start_date = dateutil.parser.parse(start_date_string).date()
                end_date = start_date + relativedelta(months=number_of_month, days=-1)

                record.start_period = start_date
                record.end_period = end_date

    @api.onchange('frequency')
    def _onchange_frequency(self):
        month = '1'
        quarter = '1'
        if self.frequency == '1':
            quarter = False
        elif self.frequency == '2':
            month = False

        self.month = month
        self.quarter_select = quarter

    # @api.onchange('start_period', 'end_period', 'hr_customer_id')
    @api.onchange('start_period', 'hr_customer_id')
    def _onchange_appraisal_bulk_customer_period(self):
        values = []
        name_display = False
        if self.hr_customer_id and self.start_period and self.end_period:
            timesheet_domain = [
                ('partner_id', '=', self.hr_customer_id.id),
                ('date', '<=', self.end_period),
                ('date', '>=', self.start_period),
            ]

            related_timesheet = self.env['account.analytic.line'].search(timesheet_domain, order='user_id')

            for user, timesheets in groupby(related_timesheet, lambda x: x.user_id):
                #enhancmenet
                list_of_date = ([timesheet.date for timesheet in timesheets])
                oldest_date = min(list_of_date)
                youngest_date = max(list_of_date)
                #end
                # list_of_month = ([timesheet.month_int for timesheet in timesheets])
                # list_of_month = list(set(list_of_month))
                # months = self.env['hr.month'].search([
                #     ('sequence', 'in', list_of_month)
                # ])
                bulk_line = (0, 0, {
                    'appraisal_partner_id': self.id,
                    'user_id': user.id,
                    # 'months': months,
                    'start_date': oldest_date,
                    'end_date': youngest_date

                })
                values.append(bulk_line)
        self.update({'hr_bulk_appraisal_ids': values})

    @api.onchange('hr_customer_id', 'frequency', 'quarter_select', 'month', 'year')
    def _onchange_parameter_customer_period(self):
        if self.hr_customer_id and self.frequency and self.year and (self.quarter_select or self.month):
            if self.frequency == '1':
                # periodic = self.month
                periodic = datetime.datetime.strptime(self.month, '%m').strftime('%b')
            elif self.frequency == '2':
                if self.quarter_select == '1': periodic = 'Quarter 1'
                elif self.quarter_select == '2': periodic = 'Quarter 2'
                elif self.quarter_select == '3': periodic = 'Quarter 3'
                elif self.quarter_select == '4': periodic = 'Quarter 4'
            name_display = 'Appraisal for {} ({} / {})'.format(self.hr_customer_id.name, periodic, self.year)
            self.name = name_display

    @api.multi
    def fetch_appraisal_reviewer(self):
        self.ensure_one()
        appraisal_reviewers = []
        if self.announcement_id and self.hr_customer_id and self.customer_survey_id:
            appraisal_reviewers.append((self.announcement_id, self.hr_customer_id, self.customer_survey_id))
        return appraisal_reviewers

    def _prepare_send_email(self):
        self.ensure_one()
        # if len(self.hr_bulk_appraisal_ids) == 0:
        #     return
        survey_url = self.customer_survey_id.public_url
        start_period_bulk = fields.Date.from_string(self.start_period).strftime('%B %d %Y')
        end_period_bulk = fields.Date.from_string(self.end_period).strftime('%B %d %Y')
        mail_content = mail_content_1 = ('Dear %(name)s, '
                          '<br/>Please fill out the following employee(s) performance related to period from %(start_period)s to %(end_period)s'
                          '<br/>'
                          '<table> '
                          '  <thead>'
                          '    <tr>'
                          '      <th>Employee</th> '
                          '      <th>Period</th>   '
                          '      <th>Survey Link</th>'
                          '    </tr> '
                          '  </thead> '
                          '  <tbody>') % {
                             'name': self.hr_customer_id.name,
                             'start_period': start_period_bulk,
                             'end_period': end_period_bulk
                         }
        for bulk_line in self.hr_bulk_appraisal_ids:
            start_date = fields.Date.from_string(bulk_line.start_date).strftime('%B %d %Y')
            end_date = fields.Date.from_string(bulk_line.end_date).strftime('%B %d %Y')
            appraisal_deadline_date = fields.Date.from_string(self.appraisal_deadline).strftime('%B %d %Y')
            period = ('%s to %s') % (start_date, end_date)
            url = survey_url + '/' + bulk_line.user_input_id.token
            url_link = ('<a href="%s">Start Survey </a>') % (url)
            mail_content_2 = ('<tr>'
                              '  <td>%(emp)s</td>'
                              '  <td>%(period)s</td>'
                              '  <td>%(url)s</td>'
                              '</tr>') % {
                                 'emp': bulk_line.user_id.name,
                                 'url': url_link,
                                 'period': period,
                             }
            mail_content_1 += mail_content_2
            mail_content_3 = ('</tbody>'
                              '</table>'
                              '<br/>'
                              '<br/> Post your response for the appraisal till: %(deadline)s') % {
                                 'deadline': appraisal_deadline_date
                             }
            mail_content = mail_content_1 + mail_content_3

        mail_subject = 'Appraisals for Visualogic Programmer - [from: %(start_period)s To: %(end_period)s]' % {
            'start_period': start_period_bulk,
            'end_period': end_period_bulk
        }
        values = {
            'model': 'hr.appraisal_bulk',
            'res_id': self.ids[0],
            'subject': mail_subject,
            'body_html': mail_content,
            'parent_id': None,
            'email_from': self.env.user.email or None,
            'email_to': self.hr_customer_id.email
        }

        mail = self.env['mail.mail'].create(values)
        return mail

    @api.multi
    def action_start_appraisal_bulk_new(self):
        """
        first time creation
        """
        self.ensure_one()
        customer_id = self.hr_customer_id
        survey_id = self.customer_survey_id

        # Create appraisal and survey user input
        for bulk_line in self.hr_bulk_appraisal_ids:
            # url = survey_id.public_url
            # employee_id = bulk_line.user_id.employee_id.id
            appraisal_value= bulk_line._prepare_appraisal()
            appraisal = self.env['hr.appraisal'].create(appraisal_value)
            response = self.env['survey.user_input'].create({
                'survey_id': survey_id.id,
                'partner_id': bulk_line.user_id.partner_id.id,
                'appraisal_id': appraisal.ids[0],
                'deadline': self.appraisal_deadline,
                'email': customer_id.email
            })
            # Set customer response id
            appraisal.write({
                'customer_response_id': response.id
            })

            # Store it on database in bulk line
            bulk_line.write({
                'user_input_id': response.id,
                'appraisal_id': appraisal.id,
            })

        ## sending email
        state = 'received'
        mail = self._prepare_send_email()
        result = mail._send()
        appraisal_state = self.env.ref('oh_appraisal.hr_appraisal_start')
        check_received = False
        failure_reason = False
        if mail.state == 'exception':
            failure_reason = mail.failure_reason
            state = 'exception'
            ##

        elif mail.state == 'sent':
            state = 'received'
            # Auto state finish when there is no bulk_line
            if not self.hr_bulk_appraisal_ids:
                state = 'done'
            check_received = True
            appraisal_state = self.env.ref('oh_appraisal.hr_appraisal_sent')
            ## update customer response id


        # Update record
        self.state = state


        self.failure_reason = failure_reason
        if result is True:
            for bulk_line in self.hr_bulk_appraisal_ids:
                bulk_line.appraisal_id.tot_sent_survey += 1
                bulk_line.appraisal_id.state = appraisal_state.id
                bulk_line.appraisal_id.check_sent = check_received
                bulk_line.appraisal_id.check_draft = not check_received

        # remove email
        mail.unlink()

    @api.multi
    def action_resend_email(self):
        self.ensure_one()
        mail = self._prepare_send_email()
        result = mail._send()
        check_received = False
        failure_reason = False
        appraisal_state = self.env.ref('oh_appraisal.hr_appraisal_start')
        if mail.state == 'exception':
            failure_reason = mail.failure_reason
            state = 'exception'

        elif mail.state == 'sent':
            state = 'received'
            check_received = True
            appraisal_state = self.env.ref('oh_appraisal.hr_appraisal_sent')

        # remove email
        mail.unlink()

        # update record
        self.state = state
        self.failure_reason = failure_reason
        if result is True:
            for bulk_line in self.hr_bulk_appraisal_ids:
                bulk_line.appraisal_id.tot_sent_survey += 1
                bulk_line.appraisal_id.state = appraisal_state.id
                bulk_line.appraisal_id.check_sent = check_received
                bulk_line.appraisal_id.check_draft = not check_received

    @api.multi
    def action_resend_email_soft(self):
        """
        Since the data has been passed to user once already..
        This method only resend data again without modify any exsiting record
        :return:
        """

        self.ensure_one()
        failure_reason = False
        mail = self._prepare_send_email()
        mail._send()
        title = 'FAIL on sending'
        if mail.state == 'exception':
            failure_reason = mail.failure_reason

        #remove email
        mail.unlink()
        self.failure_reason = failure_reason

    @api.multi
    def action_draft(self):
        """
        Delete the appraisal and survey since user never received the them
        """
        self.ensure_one()
        self.state = 'draft'
        self.failure_reason = False
        for bulk_line in self.hr_bulk_appraisal_ids:
            bulk_line.user_input_id.unlink()
            bulk_line.appraisal_id.unlink()

    @api.multi
    def action_view_appraisal(self):
        self.ensure_one()
        action = self.env.ref('oh_appraisal.hr_appraisal_action_form')
        list_view_id = self.env.ref('vlr_appraisal.hr_appraisal_evaluation_list_view').id
        form_view_id = self.env.ref('vlr_appraisal.inherit_hr_appraisal_form_view').id

        result = {
            'name': action.name,
            'type': action.type,
            'views': [(list_view_id, 'tree'), (form_view_id, 'form')],
            'target': action.target,
            'context': action.context,
            'res_model': action.res_model,
        }
        if len(self.appraisal_ids) > 1:
            appraisal_ids = list()
            for appraisal in self.appraisal_ids:
                appraisal_ids.append(appraisal.id.id)
            result['domain'] = "[('id','in',%s)]" % appraisal_ids
        elif len(self.appraisal_ids) == 1:
            result['views'] = [(form_view_id, 'form')]
            result['res_id'] = self.appraisal_ids.id.id
        else:
            result = {'type': 'ir.actions.act_window_close'}

        return result

    @api.multi
    def action_view_invoice(self):
        invoices = self.mapped('invoice_ids')
        action = self.env.ref('account.action_invoice_tree1').read()[0]
        if len(invoices) > 1:
            action['domain'] = [('id', 'in', invoices.ids)]
        elif len(invoices) == 1:
            action['views'] = [(self.env.ref('account.invoice_form').id, 'form')]
            action['res_id'] = invoices.ids[0]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action

    def _check_state_done(self):
        self.ensure_one()
        state_done = self.env['hr.appraisal.stages'].search([('sequence', '=', 3)]).id
        bulk_appraisal_done = all([bulk_line.appraisal_id.state.id == state_done for bulk_line in self.hr_bulk_appraisal_ids])

        return bulk_appraisal_done

    def action_done(self):
        self.ensure_one()
        self.state = 'done'

    @api.multi
    def unlink(self):
        for bulk in self:
            if bulk.state == 'exception':
                bulk.action_draft()
            if bulk.state == 'received' or bulk.state == 'done':
                raise UserError(_('Cannot Delete Appraisal Bulk which on received or finish state'))
        return super(HrBulkAppraisal, self).unlink()


class HrBulkAppraisalLine(models.Model):
    _name = 'hr.appraisal.bulk.line'

    appraisal_partner_id = fields.Many2one(comodel_name='hr.appraisal_bulk', string='Customer', ondelete='cascade')
    # year = fields.Integer()
    user_id = fields.Many2one(comodel_name='res.users', string='Employee')
    user_input_id = fields.Many2one(comodel_name='survey.user_input', string='User Input')
    appraisal_id = fields.Many2one(comodel_name='hr.appraisal')
    state = fields.Many2one('hr.appraisal.stages', related='appraisal_id.state', readonly=True)
    start_date = fields.Date(string='Start Period', help='start period of appraisal')
    end_date = fields.Date(string='End Period')

    def _get_bulk_state(self):
        self.ensure_one()
        state = self.appraisal_partner_id.state
        return state

    ## Enhancement
    def _prepare_appraisal(self):
        vals = {
            # 'start_date': self.start_date,
            # 'end_date:': self.end_date,
            'start_period': self.start_date,
            'end_period': self.end_date,
            'emp_id': self.user_id.employee_id.id,
            'appraisal_deadline': self.appraisal_partner_id.appraisal_deadline,
            'hr_customer': True,
            'hr_customer_id': self.appraisal_partner_id.hr_customer_id.id,
            'customer_survey_id': self.appraisal_partner_id.customer_survey_id.id,
            'hr_announcement': True,
            'announcement_id': self.appraisal_partner_id.announcement_id.id,
            'bulk_line': self.id,
        }

        return vals

