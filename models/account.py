from odoo import api, fields, models, _
import datetime

class AccountAnalyticLine(models.Model):
    _inherit = 'account.analytic.line'

    month_int = fields.Integer(compute='_compute_month_int', readonly=True,
                               help='determine timesheet falls on which month')

    @api.depends('date')
    def _compute_month_int(self):
        for timesheet in self:
            date = fields.Date.from_string(timesheet.date)
            timesheet.month_int = int(date.strftime('%m'))
