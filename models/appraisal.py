from odoo import api, fields, models, _
from odoo.exceptions import UserError

class HrAppraisalForm(models.Model):
    _inherit = 'hr.appraisal'

    bulk_line = fields.Many2one('hr.appraisal.bulk.line')

    @api.multi
    def action_done(self):
        res = super(HrAppraisalForm, self).action_done()
        if self.bulk_line:
            appraisal_state_done = self.bulk_line.mapped('appraisal_partner_id')._check_state_done()
            if appraisal_state_done:
                #Update the bulk state to done
                self.bulk_line.mapped('appraisal_partner_id').action_done()

        return res

    @api.multi
    def unlink(self):
        for appraisal in self.filtered(lambda x: x.bulk_line != False):
            state = appraisal.bulk_line._get_bulk_state()
            if state != 'draft':
                raise UserError(_('Cannot delete Appraisal Record which on Bulk Appraisal, please delete it on Bulk Appraisal Menu instaed'))

        return super(HrAppraisalForm, self).unlink()

    @api.multi
    def copy(self, default=None):
        if self.bulk_line != False:
            raise UserError(_('Cannot duplicate Appraisal Record which on Bulk Appraisal!!!'))
        return super(HrAppraisalForm, self).copy(default)
