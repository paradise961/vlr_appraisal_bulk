{
    'name': "VLR Appraisal Bulk Revampted",
    'summary': """
        Allow Bulk Appraisal for Module VLR Appraisal
    """,
    'author': "Leonardo",
    'version': '1.1',
    'description': """
        Enhancement v1.1(2019-05-17):\n
        1. v1.1. To include domain into Appraisal Bulk hr_customer_id only to customer 
        \n
        This Module Include: \n
        1. Bulk Appraisal Menu -- Which Allow Multiple Employee's Appraisal on certain period for particular Customer\n
    """,
    'depends': [
        'vlr_appraisal',
        'sale_timesheet',
    ],
    'data': [
        'views/hr_appraisal_bulk_form_views.xml',
        'views/hr_appraisal_form_views.xml',
        'views/web_assets.xml',
        'security/ir.model.access.csv',
    ],
    'qweb': [
        'static/src/xml/widget_view_1.xml',
        'static/src/xml/widget_view_2.xml',
    ],
}